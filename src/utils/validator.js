const express = require('express');
var otpGenerator = require('otp-generator');

const { BadRequest } = require('../errors/error.js');

const messageClient = require ('./twilioMsg');

async function generateAndSendOtp(req,res,next){

const generatedOtp= otpGenerator.generate(6, 
    { upperCase: false, specialChars: false , alphabets: false });

const mobileNumber= req.body.mobileNumber;

console.log("OTP for number "+mobileNumber+" is: "+generatedOtp);
console.log("sending SMS..");

var smsResult= await messageClient.sendOtp(generatedOtp,mobileNumber,next);

console.log("sms result in validator.. ");
console.log(smsResult);
return {
    smsResult,
    generatedOtp
};

}


//function to verify generatedOTP and user given OTP
function otpValidator(req,res,next){

    var twilioOtp = req.body.twilioOtp;
    var userOTP = req.body.otp;
    try{
    if(userOTP === twilioOtp ){
        console.log("otp validated..");
        next();
    }
    else{
        console.log("otp verification failed ");
        throw new BadRequest('Invalid OTP. Please try again !!!');
    }
}
catch(err){
    next(err);
}
}



module.exports={generateAndSendOtp,otpValidator};