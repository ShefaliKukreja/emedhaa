//TWILIO
//const accountSid = process.env.TWILIO_ACCOUNT_SID;
const accountSid = 'AC4e00620a2a105e5a5138e971ca63d274';
const authToken = '49986fd87f6ef401049630520444abaf';
const twilioNum= '+14422464930';
const client = require('twilio')(accountSid, authToken);

// function to send OTP to user given mobileNumber
async function sendOtp(generatedOtp,mobileNumber,next){
    var result = await client.messages
  .create({
     body: generatedOtp +' is your one time password to verify mobile number.',
     from: twilioNum,
     to: mobileNumber
   })
  .then(message =>{ return message.sid})
  .catch(function (err){
    console.log("errr in message client : "+err); 
    next(err);
  });

  return result;
}

module.exports ={sendOtp}