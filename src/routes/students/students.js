const express = require('express');
const { otpValidator } = require('../../utils/validator.js');
const otpValidation = require('../../utils/validator.js');
var registerStudentController = require('../../controller/students/registerStudent.js');
const router = express.Router();

//signUp form
router.get('/signUp', function(req, res,next){
    res.render('signUp');
  });
 
  // signUp post
router.post('/signUp', checkNumberAlreadyRegistered,async (req, res,next) => {
    try{
    const result= await otpValidation.generateAndSendOtp(req,res,next);
    if(!result.smsResult){
        next();
    }
    else{
    // passing data to another route using cookie
    res.cookie('twilioOtp', result.generatedOtp);
   res.redirect('/registerStudent');
    }
}
catch(err){
    console.log("Error reported in signup post request: "+err);
    next(err);
};
});

router.post('/registerStudent',otpValidator,(req, res,next) => {
  
    //call to controller
    console.log("sending req to student controller from router..");
    registerStudentController.registerStudent(req,res,next);
});
    
router.get('/login',(req,res,next)=>{
    //res.render('login);
})
// check if mobile number already exists
function checkNumberAlreadyRegistered(req,res,next){
// before generating otp call this middleware to check if number already existed in db
const result= 0;       // result from db 0 : no result found , 1 : record exists in db
const mobile= req.body.mobileNumber;

if(result === 1){
    res.send( mobile +" is already registered. Please login!" );
}
else{
    console.log("New user...");
    next();
}

}
    
   



module.exports = router;