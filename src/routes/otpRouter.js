const express = require('express');
const router = express.Router();
const otpValidation = require('../utils/validator.js');

router.get('/resendOtp', async function(req, res,next){
try{
    const result= await otpValidation.generateAndSendOtp(req,res,next);
    console.log("SMS status of resending OTP");
    console.log(result);

    if(!result.smsResult){
        next();
    }
    else{
    // passing data to another route using cookie
    res.cookie('twilioOtp', result.generatedOtp);
    // need only to send otp back - fe has to merge res with edit registration form 
   res.redirect('/registerStudent');
    }
}
catch(err){
    console.log("Error reported in resending OTP: "+err);
    next(err);
};
});



module.exports = router ;