const express = require('express');
const app = express();
var multer = require('multer');
var upload = multer();
var cookieParser = require('cookie-parser');
const studentRouter = require('./src/routes/students/students.js');
const handleErrors = require('./src/errors/handleError.js');
const { BadRequest } = require('./src/errors/error.js');
const otpRouter = require ('./src/routes/otpRouter.js');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(upload.array());  

// for setting view
app.set('view engine', 'pug');
app.set('views', './src/views');

app.get('/',function(req, res){
    res.send("eMedhaa: an eLearning Platform");
  });

  // GET request for registraion form
  app.get('/registerStudent', function(req, res){
  var generatedOtp = req.cookies['twilioOtp'];
  console.log("OTP for registration request: "+generatedOtp);
   res.render('form', { twilioOtp: generatedOtp });  //add null check before sending otp
 });

//GET request for signUp form
app.get('/signUp',studentRouter);

// POST request for signUp post
app.post('/signUp',studentRouter);

// GET request for resend OTP
app.get('/resendOtp',otpRouter)

//POST request for registration
app.post('/registerStudent',studentRouter);

// GET request for login page
app.get('/login',studentRouter);

//Error message for any routes not defined
app.use((req,res,next)=>{
  const error= new Error("Requested page Not found");
  //error.status=404;
  next(error);
})

// error Handler
app.use(handleErrors);

app.listen(9000,() => {
    console.log("Server started...")
  })